import pandas as pd
import streamlit as st
import plotly.express as px

df = pd.read_csv('dados.csv', header=0)

df['dia_coleta'] = pd.to_datetime(df['dia_coleta'], format='%Y-%m-%d').dt.date
df['dia_coleta'] = pd.to_datetime(df['dia_coleta'])

# df['hora_coleta'] = pd.to_datetime(df['hora_coleta'])

bottom = df.tail(1)
print(bottom)

st.sidebar.success("")

# Variaveis de amostragem
acidente = bottom['acidentes']
congestio = bottom['congest']
datas = bottom['hora_coleta'].to_string(index=False)

# Tratamento no streamlit para mostrar esses dados
col5, col6, col7 = st.columns(3)

col5.metric(label='Acidentes', value=acidente, delta='')
col6.metric(label='Congestionamentos', value=congestio, delta='')
col7.metric(label='Última Atualização', value=datas, delta='')

# Gráficos
col1, col2 = st.columns(2)

fig_congs = px.bar(bottom, x='acidentes', y=['acidentes_leves', 'acidentes_graves', 'vias_interditadas', 'acidentes_sem_tipo'], title='Acidentes por Tipo')
col1.plotly_chart(fig_congs, use_container_width=True)

fig_congs = px.bar(bottom, x='congest', y=['congest_moderado', 'congest_intenso', 'congest_parado', 'congest_sem_tipo'], title='Congestionamento por Tipo')
col2.plotly_chart(fig_congs, use_container_width=True)

fig_congs = px.bar(df, x='dia_coleta', y='acidentes', title='Acidentes Leves, Graves e Via Inter. por Total')
st.plotly_chart(fig_congs, use_container_width=True)

# fig_congs = px.pie(df, values='acidentes', names='dia_semana', title='Porcentagem de Acidentes por Dia da Semana')
# col2.plotly_chart(fig_congs, use_container_width=True)

# fig_congs = px.pie(df, values='congest', names='dia_semana', title='Congestionamento por Dia da Semana')
# col3.plotly_chart(fig_congs, use_container_width=True)

# fig_congs = px.bar(df, x='precip', y='acidentes', title='Quantidade de Acidentes por Precipitação')
# col4.plotly_chart(fig_congs, use_container_width=True)
