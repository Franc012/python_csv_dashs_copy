import requests
import urllib3
import os
import pandas as pd
import datetime
import csv

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# URLs e Headers
headers_waze = {'Referer': 'https://www.waze.com/pt-BR/live-map'}
waze_url = 'https://www.waze.com/row-rtserver/web/TGeoRSS?bottom=-3.179700819552629&left=-60.37982940673829&ma=200&mj=100&mu=20&right=-59.58881378173829&top=-2.9110532097493453&types=alerts'

# requisição api 
re = requests.get(waze_url, headers=headers_waze)
a = re.json()

# diretórios
dir_atual = os.path.dirname(os.path.realpath(__file__))
dir_csv = os.path.join(dir_atual, 'historico.csv')

# variaveis de dia
tempus = datetime.datetime.now()
day = tempus.date()

# leitura do csv
df = pd.read_csv(dir_csv, header=0)
bottom = df.tail(1)

# variaveis globais
G_acci = G_lv = G_grv = G_itd = G_nd_acci = G_cong = G_mdr = G_its = G_prd = G_nd_cong = 0

# ...
bottom_date = datetime.datetime.strptime(bottom['dia'].iloc[-1], '%Y-%m-%d').date()

def api_catch_data():
    
    acci = leve = grave = itd = nd_acci = cong = moderado = intenso = parado = nd_cong = 0
    
    for k in a['alerts']:
        if k['type'] == 'ACCIDENT':
            acci +=1
            if k['subtype'] == 'ACCIDENT_MINOR':
                leve +=1
            elif k['subtype'] == 'ACCIDENT_MAJOR':
                grave +=1
            elif k['subtype'] == 'ROAD_CLOSED_EVENT':
                itd +=1
            elif k['subtype'] == '':
                nd_acci +=1
        if k['type'] == 'JAM':
            cong+=1
            if k['subtype'] == 'JAM_MODERATE_TRAFFIC':
                moderado +=1
            elif k['subtype'] == 'JAM_HEAVY_TRAFFIC':
                intenso +=1
            elif k['subtype'] == 'JAM_STAND_STILL_TRAFFIC':
                parado +=1
            elif k['subtype'] == '':
                nd_cong +=1
    return acci, cong

if df.empty or day != bottom_date:
        
    G_acci, G_cong = api_catch_data()

    dados = [
        [G_acci, G_cong, day ]
    ]

    with open(dir_csv, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        
        for row in dados:
            writer.writerow(row)
    print('added')
else:
    
    bottom_date = datetime.datetime.strptime(bottom['dia'].iloc[-1], '%Y-%m-%d').date()
    
    if day == bottom_date:

        G_acci, G_cong = api_catch_data()

        if G_acci > bottom['acidentes'].iloc[-1] or G_cong > bottom['congest'].iloc[-1]:
            difAcci = abs(bottom['acidentes'].iloc[-1] - G_acci)
            difCong = abs(bottom['congest'].iloc[-1] - G_cong)

            a = bottom['acidentes'].iloc[-1] + difAcci
            b = bottom['congest'].iloc[-1] + difCong

            df.loc[df.index[-1], 'acidentes'] = int(a)
            df.loc[df.index[-1], 'congest'] = int(b)

            df.to_csv(dir_csv,index=False)
            print('rewriten')
        else:
            print('no adds')        